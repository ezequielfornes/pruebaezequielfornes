CREATE DATABASE prueba;

CREATE TABLE employee (
  id int(11) NOT NULL,
  name varchar(100) NOT NULL,
  salary int(14) NOT NULL,
  dept_id int(6) NOT NULL,
  PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE department (
  id int(11) NOT NULL,
  name varchar(80) NOT NULL,
  location varchar(100) NOT NULL,
  PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



INSERT INTO `employee` (`id`, `name`,`salary`,`dept_id`) VALUES
(1, 'Candice',4685,1),
(2, 'Julia',2559,2),
(3, 'Bob',4405,4),
(4, 'Scarlet',2350,1),
(5, 'Ileana',1151,4);



INSERT INTO `department` (`id`, `name`, `location`) VALUES
(1, 'Executive', 'Sydney'),
(2, 'Production', 'Sydney'),
(3, 'Resources', 'Cape Town'),
(4, 'Technical', 'Texas'),
(5,'Management','Paris');

-- QUERY 
SELECT department.name, COUNT(employee.dept_id) as employees
FROM department
LEFT JOIN employee 
ON (department.id = employee.dept_id)
GROUP BY department.name
ORDER BY employees DESC, department.name ASC;