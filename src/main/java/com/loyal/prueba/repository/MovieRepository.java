package com.loyal.prueba.repository;

import com.loyal.prueba.model.Movies;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovieRepository extends JpaRepository<Movies, Long> {

}
