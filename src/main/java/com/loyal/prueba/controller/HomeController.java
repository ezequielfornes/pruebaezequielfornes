package com.loyal.prueba.controller;

import com.loyal.prueba.model.Movies;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
	
	@RequestMapping("/home")
	public String home(Model model) {
		model.addAttribute("allMovies");
		return "index";
	}
	@PostMapping("api/movie")
	public Movies mostrarMovies(@RequestBody Movies movie) {
		System.out.println(movie);
		return movie;
	}

}
