package com.loyal.prueba.controller;

import com.loyal.prueba.model.ListMovies;
import com.loyal.prueba.model.Movies;
import com.loyal.prueba.model.Protagonist;
import com.loyal.prueba.service.MovieService;
import com.loyal.prueba.service.impl.ConsumingRestApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@RestController
public class MovieController {

    @Autowired
    private MovieService movieService;

    @GetMapping("/movie")
    public ListMovies obtenerMovies(Protagonist protagonist){
        ConsumingRestApplication consumingRestApplication=new ConsumingRestApplication(protagonist.getName());
        RestTemplateBuilder builder = new RestTemplateBuilder();
        return consumingRestApplication.run(consumingRestApplication.restTemplate(builder));
    }

    @GetMapping("/peliculas")
    public String peliculas(@RequestAttribute("name") Protagonist protagonist, Model model) throws Exception {
        model.addAttribute("getPeliculas", movieService.getMovieTitles(protagonist));
        return "index";
    }

    @PostMapping("api/movie")
    public Movies mostrarMovies(@RequestBody Movies movie) throws Exception {
        System.out.println(movie);
        movieService.guardarMovie(movie);
        return movie;
    }
}
