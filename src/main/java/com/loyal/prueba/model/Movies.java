package com.loyal.prueba.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "movies")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Movies {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@JsonProperty("Title")
	private String title;
	@JsonProperty("Year")
	private Integer year;
	private String imdbID;

	public Movies (){}

	public Movies(String title, Integer Year, String imdbID) {
		this.title = title;
		this.year = Year;
		this.imdbID = imdbID;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitleData(String title) {
		this.title = title;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer Year) {
		this.year = Year;
	}

	public String getImdbID() {
		return imdbID;
	}

	public void setImdbID(String imdbID) {
		this.imdbID = imdbID;
	}

	@Override
	public String toString() {
		return "Movies{" +
				"Title='" + this.title + '\'' +
				", Year=" + this.year +
				", ImbID=" + this.imdbID +
				'}';
	}
}