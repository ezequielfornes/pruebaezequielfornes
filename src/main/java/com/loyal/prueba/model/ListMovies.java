package com.loyal.prueba.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;


@JsonIgnoreProperties(ignoreUnknown = true)
public class ListMovies {
    Integer page;
    Integer per_page;
    Integer total_page;
    Integer total;
    ArrayList<Movies> data;


    public ListMovies(){}

    public ListMovies(Integer page, Integer perPage, Integer total, Integer total_pages, ArrayList<Movies> data) {
        this.page = page;
        this.per_page = perPage;
        this.total_page = total_pages;
        this.total = total;
        this.data = data;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPer_page() {
        return per_page;
    }

    public void setPer_page(Integer per_page) {
        this.per_page = per_page;
    }

    public Integer getTotal_pages() {
        return total_page;
    }

    public void setTotal_pages(Integer total_page) {
        this.total_page = total_page;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public ArrayList<Movies> getData() {
        return data;
    }

    public void setData(ArrayList<Movies> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "List Movies{" +
                "Page='" + page + '\'' +
                ", Per page=" + per_page +
                ", Total Pages=" + total_page +
                ",Total="+total+
                ", Data:{"+
                dataToString()+
                '}';
    }

    private String dataToString() {
        String listaPelis=null;
        for (Movies mov : this.getData()) {
            listaPelis +="\n"+ mov.toString();
        }
        return listaPelis;
    }
}
