package com.loyal.prueba;

import com.loyal.prueba.model.ListMovies;
import com.loyal.prueba.model.Protagonist;
import com.loyal.prueba.service.impl.ConsumingRestApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;

@SpringBootApplication
public class Application {


	public static void main(String[] args) {
		//SpringApplication.run(Application.class, args);
		Protagonist protagonist = new Protagonist("spiderman");
		RestTemplateBuilder builder = new RestTemplateBuilder();
		ListMovies listMovies;
		ConsumingRestApplication consumingRestApplication = new ConsumingRestApplication(protagonist.getName());
		listMovies=consumingRestApplication.run(consumingRestApplication.restTemplate(builder));


	}
}
