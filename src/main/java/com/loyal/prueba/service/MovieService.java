package com.loyal.prueba.service;

import com.loyal.prueba.model.ListMovies;
import com.loyal.prueba.model.Movies;
import com.loyal.prueba.model.Protagonist;

public interface MovieService {

	public ListMovies getMovieTitles(Protagonist protagonist) throws Exception;
	public void guardarMovie(Movies movie) throws Exception;

}
