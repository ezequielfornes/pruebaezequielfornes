package com.loyal.prueba.service.impl;

import com.loyal.prueba.model.ListMovies;
import com.loyal.prueba.model.Movies;
import com.loyal.prueba.model.Protagonist;
import com.loyal.prueba.repository.MovieRepository;
import com.loyal.prueba.service.MovieService;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;

@Service
public class MovieServiceImpl implements MovieService {

	private MovieRepository movieRepository;


	@Override
	public void guardarMovie(Movies movie) throws Exception {
		movieRepository.save(movie);
	}

	@Override
	public ListMovies getMovieTitles(Protagonist protagonist) throws Exception {
		RestTemplateBuilder builder = new RestTemplateBuilder();
		ListMovies listMovies;
		ConsumingRestApplication consumingRestApplication = new ConsumingRestApplication(protagonist.getName());
		listMovies=consumingRestApplication.run(consumingRestApplication.restTemplate(builder));
		return listMovies;
	}



}
