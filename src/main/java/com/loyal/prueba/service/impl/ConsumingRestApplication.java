package com.loyal.prueba.service.impl;

import com.loyal.prueba.model.ListMovies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;


public class ConsumingRestApplication {

    private String name;
    private Integer page=1;

    public ConsumingRestApplication(String name) {
        this.name = name;
    }

    private static final Logger log = LoggerFactory.getLogger(ConsumingRestApplication.class);


    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    public ListMovies run(RestTemplate restTemplate)  {


            ListMovies listMovies = (ListMovies)restTemplate.getForObject("https://jsonmock.hackerrank.com/api/movies/search/?Title=" + name + "&page=" + page,
                    ListMovies.class);
            log.info(listMovies.toString());
            //System.out.println(listMovies.toString());
            return listMovies;

    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPage(Integer page) {
        this.page = page;
    }
}
